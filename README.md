NVIDIA's EGLStream weston patches are no longer maintained by mvicomoya
=======================================================================

If you are looking for the latest weston version with EGLStream patches, go to [Erik Kurzinger's fork](https://gitlab.freedesktop.org/ekurzinger/weston).
